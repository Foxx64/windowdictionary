import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JPanel;


public class LearnPhrasePanel {
	static JPanel createLearnPhrasePanel (ArrayList <Card> words) throws SQLException{
		JPanel learnPhrasePanel = LearnCardPanel.createlearnCardPanel(words);
		return learnPhrasePanel;
	}

	
}
