import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class AddWordPanel {

	static JPanel createAddWordPanel () {
		
		JPanel addWordPanel = new JPanel();// панель добавления слова
		JPanel englishWordPanel = new JPanel ();
		addWordPanel.setLayout(new BoxLayout (addWordPanel, BoxLayout.Y_AXIS));
		JLabel englishWordLabel  = createEnglishWord();
		englishWordPanel.add(englishWordLabel);
		addWordPanel.add(englishWordPanel);
		
		JPanel wordPanel = new JPanel();// контейнер под лейбл и текст филд для ввода слова
	
		JLabel wordLabel = createWordLabel();
		wordPanel.add(wordLabel);
		
		final JTextField wordTextField = new JTextField (20);
		wordPanel.add(wordTextField);
		addWordPanel.add(wordPanel);
		
		JPanel translationPanel = new JPanel();// контейнер под лейбл и текст филд для ввода перевода
		JLabel translationLabel = createTranslationLabel();
		translationPanel.add(translationLabel);
		
		final JTextField translationTextField = new JTextField (20);
		translationPanel.add(translationTextField);
		addWordPanel.add(translationPanel);
		JPanel addButtonPanel = new JPanel();
		JButton addButton = createAddButton(wordTextField, translationTextField, englishWordLabel);
		addButtonPanel.add(addButton);
		addWordPanel.add(addButtonPanel);
		return addWordPanel;
	}

	private static JLabel createEnglishWord(){
		JLabel englishWordLabel = new JLabel("Введите пожалуйста английское слово ");
		Dimension wordSize = new Dimension(350, 20);
		Font font = new Font("Verdana", Font.PLAIN, 15);
		englishWordLabel.setHorizontalAlignment(JLabel.CENTER);
		englishWordLabel.setPreferredSize(wordSize);
		englishWordLabel.setForeground(Color.BLUE);
		englishWordLabel.setFont(font);
		return englishWordLabel;
	}
	
	
	/***
	 * создает JLabel который выводит сообщение о неоходимости ввода слов
	 */
	private static JLabel createWordLabel() {
		Font font = new Font("Verdana", Font.PLAIN, 15);
		JLabel label = new JLabel("Слово");
		label.setHorizontalAlignment(JLabel.LEFT);
		Dimension size = new Dimension(60, 20);
		label.setPreferredSize(size);
		label.setForeground(Color.BLACK);
		label.setFont(font);
		return label;
	}
	
	private static JLabel createTranslationLabel() {
		JLabel labelTranslation = new JLabel("Перевод");
		
		labelTranslation.setHorizontalAlignment(JLabel.LEFT);
		Dimension labelTranslationSize = new Dimension(70, 30);
		labelTranslation.setPreferredSize(labelTranslationSize);
		labelTranslation.setForeground(Color.BLACK);
		Font fontNameTranslation = new Font("Verdana", Font.PLAIN, 15);
		labelTranslation.setFont(fontNameTranslation);
		return labelTranslation;
	}
	
	/*
	 * создает готовую кнопку для добавления нового слова (вместе с событием)
	 */
	public static JButton createAddButton(final JTextField wordTextField, final JTextField translationTextField, 
			 final JLabel englishWordLabel) {
		JButton addButton = new JButton ("Добавить слово и перевод");
		addButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent addWordActionEvent) {
				String word = wordTextField.getText();
				String translation = translationTextField.getText();
				try {
					if (Data.contains(word)){
						englishWordLabel.setText("это слово уже есть в словаре");
					} else {
						Data.createCard(word, translation);
					}
					translationTextField.setText("");
					wordTextField.setText("");
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}
		});
		return addButton;
	} 
	

}
