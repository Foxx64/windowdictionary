import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class WordList {
	static JPanel createWordListPanel (){
		JPanel wordListPanel = new JPanel();
		wordListPanel.setLayout(new BoxLayout(wordListPanel, BoxLayout.Y_AXIS));
		final ArrayList<Card> cards = Data.getUnlearnedCards();
		for (int i = 0; i < cards.size(); i++){
			final Card card = cards.get(i);
			final JLabel wordLable = createWordLable(card.getWord());
			JPanel buttonPanel = new JPanel ();
			buttonPanel.add(wordLable);
			final JButton button  = new JButton ("Удалить");
			buttonPanel.add(button);
			wordListPanel.add(buttonPanel);
			button.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent removeWordActionEvent) {
					Data.deleteCardByID(card.getId());
					JPanel buttonPanel = new JPanel();
					buttonPanel.remove(wordLable);
					buttonPanel.remove(button);
					buttonPanel.repaint();
				}
			});
		}

		int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
		int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
		JScrollPane jsp = new JScrollPane(wordListPanel,v,h);
		JPanel containerWordListPanel = new JPanel();
		containerWordListPanel.setLayout(new BorderLayout());
		containerWordListPanel.add("Center", jsp);
		return containerWordListPanel;
	}

	private static JLabel createWordLable(String word) {
		JLabel wordLable = new JLabel(word);
		Dimension wordSize = new Dimension(150, 15);
		wordLable.setPreferredSize(wordSize);
		Font font = new Font("Verdana", Font.PLAIN, 15);
		wordLable.setHorizontalAlignment(JLabel.CENTER);
		wordLable.setForeground(Color.BLUE);
		wordLable.setFont(font);
		return wordLable;
	}
	
}
