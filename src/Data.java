
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Data {
	public static Connection getConnection() throws SQLException{
		return  DriverManager.getConnection(
				"jdbc:oracle:thin:@localhost:1521:XE", "SYS as SYSDBA",
				"123");
	}
	
	public static void loadDriver(){
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Card> getCards(String wordQuery) {
		ArrayList<Card> cards = new ArrayList<>();
		try {
			Connection connection = getConnection();
			Statement st = connection.createStatement();
			ResultSet resultSet = st.executeQuery(wordQuery);
			while (resultSet.next()){
				Card card = new Card();
				int id = resultSet.getInt("ID");
				card.setId(id);
				String w = resultSet.getString("WORD");
				card.setWord(w);
				String t = resultSet.getString("TRANSLATION");
				card.setTranslation(t);
				int answersCount = resultSet.getInt("ANSWERSCOUNT"); 
				card.setCorrectAnswersCount(answersCount);
				cards.add (card);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cards;
	}
	
	public static ArrayList<Card> getWordCards() {
		return getCards("select ID, WORD, TRANSLATION, ANSWERSCOUNT from cards where word not like + '% %'");
	}

	public static ArrayList<Card> getPhraseCards() {
		return getCards("select ID, WORD, TRANSLATION, ANSWERSCOUNT from cards where word like + '% %'");
	}
	
	public static ArrayList<Card> getUnlearnedCards() {
		return getCards("select * from cards where answerscount < 3");
	}
	
	
	public static void deleteCardByID (int cardID) {
		try {
			Connection connection = getConnection();
			Statement st = connection.createStatement();
			String query = String.format("delete from cards where ID = %d", cardID);
			st.execute(query);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void increaseAnswerscount (int cardID, int answersCount){
		try {
			Connection connection = getConnection();
			Statement st = connection.createStatement();
			if (answersCount <= 0 || answersCount < 3){
				answersCount ++;
			}
			String query = String.format("update cards set answerscount = " + answersCount + " where ID =" + cardID);
			st.execute(query);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean contains (String word) throws SQLException{
		ResultSet resultSet = null;
		Connection connection = getConnection();
		Statement st = connection.createStatement();
		String query = "select ID from cards where word = '" + word + "'";
		resultSet = st.executeQuery(query);
		return resultSet.next();
	}
	
	public static void createCard(String word, String translation){
		Connection connection;
		try {
			connection = getConnection();
			String query = "insert into cards"
			+ " Values ( '"+ word + "', '" + translation + "', '0', Cards_seq.nextval)";
			Statement st = connection.createStatement();
			st.execute(query);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
