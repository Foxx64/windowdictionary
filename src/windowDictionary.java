import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
public class WindowDictionary extends JFrame {
	static ArrayList<Card> cards = new ArrayList<>();
	static int i = 0;

	public WindowDictionary() throws IOException {
		super("Cловарик");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		JPanel contentPanel = createContentPanel();
		getContentPane().add(contentPanel);
		setPreferredSize(new Dimension(600, 380));
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				System.exit(0);
			}
		});
	}

	private JPanel createContentPanel() throws IOException {
		JPanel contentPanel = createLineWordPanel();
		contentPanel.setLayout(new BorderLayout());
		
		Font font = new Font("Verdana", Font.PLAIN, 10);
		final JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.setFont(font);
		
		JPanel addWordPanel = AddWordPanel.createAddWordPanel();
		tabbedPane.addTab("Добавить карточку", addWordPanel);
		contentPanel.add(tabbedPane);
		
		final JPanel tabbedPaneContainerPanel = new JPanel();
		tabbedPane.addTab("Учить слова", tabbedPaneContainerPanel);
		final JPanel learnPhraseСontainerPanel = new JPanel();
		tabbedPane.add("Учить фразы", learnPhraseСontainerPanel);
		tabbedPane.addChangeListener(new ChangeListener(){
		public void stateChanged(ChangeEvent e){
				JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
			try {
				if (tabbedPane.getSelectedIndex() == 1){
					ArrayList<Card>	wordsList  = new ArrayList<>();	
					wordsList = Data.getWordCards();
					tabbedPaneContainerPanel.removeAll();
					JPanel wordListPanel =  LearnCardPanel.createlearnCardPanel(wordsList);
					tabbedPaneContainerPanel.add(wordListPanel);
				} else if (tabbedPane.getSelectedIndex() == 2){
					learnPhraseСontainerPanel.removeAll();
					ArrayList<Card> cards = Data.getPhraseCards();
					JPanel phraseListPanel =  LearnPhrasePanel.createLearnPhrasePanel(cards);
					learnPhraseСontainerPanel.add(phraseListPanel);
				}
			} catch (SQLException e1) {
						e1.printStackTrace();
			}
		}

		});
		contentPanel.add(tabbedPane);
		JPanel wordListPanel = WordList.createWordListPanel ();
		tabbedPane.add("Показать список невыученных карточек", wordListPanel);
		contentPanel.add(tabbedPane);
		return contentPanel;
	}

	private JPanel createLineWordPanel() {
		JPanel lineWordsPanel = new JPanel();
		return lineWordsPanel;
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Data.loadDriver();
		WindowDictionary dictioneryPanel = new WindowDictionary();
		

	}

}
