import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class LearnCardPanel {
	
	
	public static JPanel  createlearnCardPanel (ArrayList<Card> cards)
	{
		JPanel controlContainerPanel = new JPanel();
		controlContainerPanel.setLayout(new BoxLayout(controlContainerPanel, BoxLayout.Y_AXIS)); 
		
		int unlearnedIndex = getUnlernedCardIndex(cards);
		int sizeCards = cards.size();
		boolean canLearn = getCanLearn(sizeCards, unlearnedIndex, controlContainerPanel);
		if (!canLearn){
			return controlContainerPanel;
		}
		ArrayList<Integer> randomNumbers = getMixedArray(cards, unlearnedIndex);
		String translation = cards.get(unlearnedIndex).getTranslation();
		
		JLabel enterTranslationLabel = createEnterTranslationLabel();
		enterTranslationLabel.setText("Введите пожалуйста перевод карточки: " + translation);
		
		JPanel enterTranslationPanel = createEnterTranslationPanel(enterTranslationLabel, controlContainerPanel, unlearnedIndex);
		controlContainerPanel.add(enterTranslationPanel);
		
		JPanel wordListPanel = new JPanel ();
		
		wordListPanel.setLayout(new BoxLayout(wordListPanel, BoxLayout.Y_AXIS)); 
		
		JTextField enterWordField  = new JTextField(25);
		JPanel containerWordEndFieldPanel = new JPanel();
		
		JLabel enterWordLabel = createEnterWordLable();
		enterLearnWordTraslation(controlContainerPanel, enterWordField, containerWordEndFieldPanel,
				unlearnedIndex, enterWordLabel, cards );
		for (int i = 0; i < randomNumbers.size(); i++){
			Card card = cards.get(randomNumbers.get(i));
			String word = card.getWord();
			JLabel labelWord = createLableWord(word);
			wordListPanel.add(labelWord);
		}
		JPanel wordListContainerPanel = new JPanel();
		wordListContainerPanel.setLayout(new BoxLayout(wordListContainerPanel, BoxLayout.Y_AXIS)); 
		wordListContainerPanel.add(containerWordEndFieldPanel);
		JPanel containerListPanel = new JPanel();
		wordListContainerPanel.add(enterWordLabel);
		containerListPanel.add(wordListPanel);
		wordListContainerPanel.add(containerListPanel);
		controlContainerPanel.add(wordListContainerPanel);
		controlContainerPanel.add(containerListPanel);
	
		return controlContainerPanel;
	}

	private static JLabel createLableWord(String word) {
		JLabel labelWord = new JLabel(word);
		Dimension wordSize = new Dimension(150, 50);
		labelWord.setPreferredSize(wordSize);
		Font font = new Font("Verdana", Font.PLAIN, 15);
		labelWord.setHorizontalAlignment(JLabel.CENTER);
		labelWord.setForeground(Color.BLACK);
		labelWord.setFont(font);
		return labelWord;
	}
	
	private static void enterLearnWordTraslation(JPanel controlContainerPanel, JTextField enterWordField, 
			JPanel containerWordEndFieldPanel, int unlearnedIndex, JLabel enterWordLabel, ArrayList<Card> cards) {
		JPanel containerTextField = new JPanel ();
		containerTextField.add(enterWordField);
		controlContainerPanel.add(containerTextField);
		containerTextField.add(enterWordField);
		containerWordEndFieldPanel.add(containerTextField);
		JButton enterСheckWordButton = createEnterСheckWordButton(enterWordLabel, enterWordField, cards, 
				containerWordEndFieldPanel, unlearnedIndex, controlContainerPanel);
		containerWordEndFieldPanel.add(enterСheckWordButton);
		
	}

	private static boolean getCanLearn (int sizeCards, int unlearnedIndex, JPanel controlContainerPanel ){
		if (unlearnedIndex < 0){
			JLabel wordLabel = createMessageAllLearned();
			controlContainerPanel.add(wordLabel);
			return false;
			
		} else if (sizeCards < 5){
			JLabel wordLabel = createMessageMoreWords(sizeCards);
			controlContainerPanel.add(wordLabel);
			return false;
		}
		return true;
	}
	
	private static JLabel createEnterWordLable() {
		JLabel enterWordLabel = new JLabel( );
		Dimension enterWordLabelSize = new Dimension(60, 30);
		enterWordLabel.setPreferredSize(enterWordLabelSize);
		enterWordLabel.setForeground(Color.RED);
		Font enterWordFont = new Font("Verdana", Font.PLAIN, 14);
		enterWordLabel.setFont(enterWordFont);
		return enterWordLabel;
	}

	private static JLabel createMessageAllLearned() {
		Dimension labelSize = new Dimension(250, 80);
		Font fontWord = new Font("Verdana", Font.PLAIN, 16);
		JLabel wordLabel = new JLabel("Вы выучили все карточки");
		wordLabel.setHorizontalAlignment(JLabel.CENTER);
		wordLabel.setPreferredSize(labelSize);
		wordLabel.setForeground(Color.BLUE);
		wordLabel.setFont(fontWord);
		return wordLabel;
	}

	private static JLabel createMessageMoreWords(int cardsSize) {
		Dimension labelSize = new Dimension(300, 150);
		Font wordFont = new Font("Verdana", Font.PLAIN, 14);
		int residue = 5 - cardsSize;
		JLabel wordLabel = new JLabel("Введите пожалуйста еще " + residue + "карточки" );
		wordLabel.setHorizontalAlignment(JLabel.CENTER);
		wordLabel.setPreferredSize(labelSize);
		wordLabel.setForeground(Color.BLACK);
		wordLabel.setFont(wordFont);
		return wordLabel;
	}

	private static JPanel createEnterTranslationPanel(JLabel enterTranslationLabel, 
			JPanel learnWordPanel, int unlearnedIndex)
	{
		JPanel enterTranslationPanel = new JPanel();
		enterTranslationPanel.setPreferredSize(new Dimension(250, 40));
		enterTranslationPanel.setLayout(new BoxLayout(enterTranslationPanel, BoxLayout.X_AXIS));
		enterTranslationPanel.add(enterTranslationLabel);
		return enterTranslationPanel;
	}

	public static JButton createEnterСheckWordButton(final JLabel enterWordLable, final JTextField enterWordField,
			final ArrayList<Card> cards, final JPanel wordListPanel, final int unlearnedIndex, final JPanel controlContainerPanel) 
	{
		JButton enterСheckWordButton = new JButton ("Проверить");
		enterСheckWordButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent learnWordActionEvent) {
				Card unlearnedCard = cards.get(unlearnedIndex);
				String learnWord = enterWordField.getText();
				enterWordField.setText(" ");
				if (new String(learnWord).equals(unlearnedCard.getWord())){
					int correctAnswersCount = unlearnedCard.getCorrectAnswersCount();
					unlearnedCard.setCorrectAnswersCount(correctAnswersCount+1);
					enterWordLable.setText("правильно !");
					LearnCardPanel.createlearnCardPanel(cards);
					Data.increaseAnswerscount (unlearnedCard.getId(), correctAnswersCount);					
				} else{
					enterWordLable.setText("не правильно , правильный ответ: " + unlearnedCard.getWord());
				}
			}
		});
		return enterСheckWordButton;
	}

	private static JLabel createEnterTranslationLabel() {
		Dimension wordSize = new Dimension(250, 20);
		Font font = new Font("Verdana", Font.PLAIN, 14);
		JLabel learnedWordLabel = new JLabel();
		learnedWordLabel.setHorizontalAlignment(JLabel.CENTER);
		learnedWordLabel.setPreferredSize(wordSize);
		learnedWordLabel.setForeground(Color.BLUE);
		learnedWordLabel.setFont(font);
		return learnedWordLabel;
	}
	
	
	private static ArrayList<Card> getUnlearnedCardList (ArrayList<Card> cards){
		ArrayList <Card> unlernedCards = new ArrayList <>();
		for (int i = 0; i < cards.size(); i++){
			if (cards.get(i).getCorrectAnswersCount() < 3 ){
				unlernedCards.add(cards.get(i));
			}
		}
		return unlernedCards;	
	}
	
	public static int getUnlernedCardIndex (ArrayList<Card> cards)
	{
		ArrayList<Card> unlernedCards = getUnlearnedCardList(cards);
		for (int i = 0; i < cards.size(); i++){
			if (unlernedCards.size() != 0){
				int sizeCards = unlernedCards.size();
				int randomIndex = (int)(Math.random() * sizeCards);
				Card lernCard = unlernedCards.get(randomIndex );
				int indexCard = cards.indexOf(lernCard);
				return indexCard;
			}
		}
		return -1;
	}
	
	/***
	 * возвращает массив случайных чисел
	 * @param wordsSize максимальное случайное число
	 * @param randomNumbersAmount количество случайных чисел
	 * @param translationIndex номер изучаемого слова
	 */
	private static int[] getRandomIndexArray(int wordsSize, int randomNumbersAmount, int translationIndex) {
		int[] randomNumbers = new int [randomNumbersAmount];
		for (int k = 0; k < randomNumbersAmount; k++){
			// выбираем новое случайное число, которое не совпадает с уже выбранными и не совпадает с номером изучаемого слова
			do{
				randomNumbers [k] = ((int)(Math.random()*wordsSize));
			} while (contains(randomNumbers, k) || randomNumbers [k] == translationIndex );
		}
		return randomNumbers;
	}
	
	private static boolean contains(int[] randomNumbers, int k ) {
		for(int j = 0; j < k; j++){
			if (randomNumbers [k] == randomNumbers [j] ){
				return true;
			}
		}
		return false;
	}
	
	public static ArrayList <Integer> getMixedArray ( ArrayList<Card> cards, int unlearnedIndex ){
		ArrayList<Integer> randomIndexCard = new ArrayList <>();
		int loopsNumber = 4;
		int sizeCards = cards.size();
		int[] randomNumbers = getRandomIndexArray(sizeCards, loopsNumber, unlearnedIndex);
		for (int i = 0; i < randomNumbers.length; i++){
			randomIndexCard.add(randomNumbers [i]);
		}
		randomIndexCard.add(unlearnedIndex);
		int randomNumber = (int)(Math.random() * loopsNumber );
		int variable = randomIndexCard.get(randomIndexCard.size()-1);
		randomIndexCard.set(randomIndexCard.size()-1,randomIndexCard.get(randomNumber));
		randomIndexCard.set(randomNumber, variable);
		return randomIndexCard;
	}
	
	public static String getStackTraceString(Exception x){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		x.printStackTrace(pw);
		return sw.toString();
	}
}
